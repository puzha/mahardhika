<?php

class Article extends MY_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index($hlm = 1)
    {
        $this->data['jsdepan'][] = base_url('assets/mahardhika/js/article.js');
        
        $data = [
            'categories' => $this->global_model->_get('categories')
        ];
        
        $this->load->templateFront('article/grid', $data);
    }

    function load_article($hlm = 1, $filterisasi = NULL)
    {
        $data = [
            'articles' => $this->global_model->_retrieve(
                $table = 'articles',
                $select = '*',
                $colOrder     = array('id'),
                $filter       = array(),
                $where        = NULL,
                $like_value   = NULL, 
                $column_order = 0, 
                $column_dir   = 'DESC', 
                $limit_start  = 0, 
                $limit_length = 8,
                $group_by     = NULL
            )
        ];

        $this->load->view('mahardhika/article/article-list', $data);
    }

}

?>
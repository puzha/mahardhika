<?php

class Articles extends MY_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index($hlm = 1)
    {
        $this->data['css'][] = base_url('assets/custom/css/custom.css');
        $this->data['js'][] = base_url('assets/custom/js/articles/article-lists.js');
        
        $data = [
            'categories' => $this->global_model->_get('categories')
        ];
        
        $this->load->templateAdmin('articles/list', $data);
    }

    function article_list($hlm = 1, $cat = NULL)
    {
        $articles = $this->global_model->_retrieve(
            'articles', 
            'id, judul, title, gambar, created_by, created_at, categories, tags, content, isi',
            [
                'id'
            ],
            [],
            NULL,
            NULL,
            0, 
            'ASC', 
            0, 
            20,
            NULL
        );

        $data = [
            'articles' => $articles,
            'start' => ($hlm * 20) - 19,
            'end' => $hlm * 20,
        ];

        $this->load->view('popoti/articles/article-lists', $data);
    }

    function post_article($id = NULL)
    {
        $this->data['css'][] = base_url('assets/libs/summernote/summernote-bs4.min.css');
        $this->data['css'][] = base_url("assets/libs/dropzone/min/dropzone.min.css");
        $this->data['css'][] = base_url("assets/libs/dropify/css/dropify.min.css");
        $this->data['css'][] = base_url('assets/libs/selectize/css/selectize.bootstrap3.css');
        $this->data['js'][] = base_url('assets/libs/summernote/summernote-bs4.min.js');
        $this->data['js'][] = base_url('assets/custom/js/about-us/summernote.init.js');
        $this->data['js'][] = base_url("assets/libs/dropzone/min/dropzone.min.js");
        $this->data['js'][] = base_url("assets/libs/dropify/js/dropify.min.js");
        $this->data['js'][] = base_url('assets/libs/devbridge-autocomplete/jquery.autocomplete.min.js');
        $this->data['js'][] = base_url('assets/libs/selectize/js/standalone/selectize.min.js');
        $this->data['js'][] = base_url('assets/custom/js/articles/post-article.js');

        if($id):
            $id = decode($id);

            $article = $this->global_model->_get('articles', ['id' => $id])->row();
        else:
            $article = (object) array();
        endif;
        
        $data = [
            'article' => $article
        ];
        
        $this->load->templateAdmin('articles/compose', $data);
    }

    function categories_data() {
        $query = $_REQUEST['query'];
        $categories = $this->db->like('name', $query)->get('categories');

        $cats = array();
        
        if($categories->num_rows() > 0):
            foreach($categories->result() as $cat):
                $cats[] = ['id' => $cat->id, 'value' => $cat->name];
            endforeach;
        endif;

        echo json_encode(['suggestions' => $cats]);
    }

    function add_category() {
        $data = [];
        
        $this->load->view('popoti/articles/new-category', $data);
    }

    function save_category() {
        $this->form_validation->set_rules('name', 'Category Name', 'required|max_length[50]');

        if ($this->form_validation->run() != false):
            $name = $this->input->post('name');

            $insert = $this->global_model->_insert('categories', ['name' => $name]);
            if ($this->db->affected_rows() > 0):
                echo json_encode([
                    'status' => 1,
                    'pesan' => 'New data has been added',
                    'id' => $insert,
                ]);
            else:
                echo json_encode([
                    'status' => 0,
                    'pesan' => 'System Error'
                ]);
            endif;
        else:
            $this->input_error();
        endif;
    }

    function save() {
        $this->form_validation->set_rules('judul', 'Judul', 'required|max_length[200]');
        $this->form_validation->set_rules('title', 'Title', 'required|max_length[200]');
        $this->form_validation->set_rules('content', 'Content English Version', 'required');
        $this->form_validation->set_rules('isi', 'Content Indonesia Versi', 'required');
        $this->form_validation->set_rules('categories', 'Category', 'required');
        $this->form_validation->set_rules('tags', 'Tags', 'required');

        if($this->form_validation->run() == true):
            $gambar = $_FILES['gambar']['size'];
            
            $config['upload_path']   = './media/article/';
            $config['allowed_types'] = 'png|jpg|jpeg';
            $config['max_size']      = 3000;

            $tags = explode(',', $this->input->post('tags'));

            $insert = [
                'judul' => $this->input->post('judul'),
                'title' => $this->input->post('title'),
                'content' => $this->input->post('content'),
                'isi' => $this->input->post('isi'),
                'categories' => $this->input->post('categories'),
                'tags' => json_encode($tags),
                'created_by' => $this->session->userdata('user'),
                'created_at' => date('Y-m-d H:i:s')
            ];
        
            $this->load->library('upload', $config);

            if ($gambar <= 0 && !$this->input->post('id')):
                $this->query_error('Please upload 1 gambar');
                die();
            elseif ($gambar > 0 && ! $this->upload->do_upload('gambar')):
                $this->query_error($this->upload->display_errors());
                die();
            elseif ($gambar > 0):
                $upload_photo = $this->upload->data();
                $insert['gambar'] = $upload_photo['file_name'];
            endif;

            if ($this->input->post('id')):
                $this->global_model->_update('articles', $insert, ['id' => decode($this->input->post('id'))]);
                $save = $this->db->affected_rows();
                saveSlug('articles/detail/'.$this->input->post('id'), create_slug($this->input->post('title')));
                saveSlug('articles/detail/'.$this->input->post('id'), create_slug($this->input->post('judul')));
            else:
                $save = $this->global_model->_insert('articles', $insert);
                saveSlug('articles/detail/'.encode($save), create_slug($this->input->post('title')));
                saveSlug('articles/detail/'.encode($save), create_slug($this->input->post('judul')));
            endif;

            if($save > 0):
                echo json_encode([
                    'status' => true,
                    'pesan' => 'Data has been updated'
                ]);
            else:
                echo json_encode([
                    'status' => 0,
                    'pesan' => 'There is nothing to change'
                ]);
            endif;
        else:
            $this->input_error();
        endif;        
    }

    function detail($id = NULL) {
        if ($id):
            $id = decode($id);

            $data = array();
            
            $this->load->templateFront('article/grid', $data);
        endif;
    }
    
}

?>
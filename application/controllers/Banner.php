<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends MY_Controller {
    function __construct()
    {
        parent::__construct();
    }

    function index() {
        isloggedin();

        // cekoto('banner', 'view', true, true);

        $data = [
            'data' => $this->global_model->_get('banner')
        ];

        $this->load->templateAdmin('banner/list', $data);
    }

    function new() {
        
        $this->data['css'][] = base_url("assets/libs/dropzone/min/dropzone.min.css");
        $this->data['css'][] = base_url("assets/libs/dropify/css/dropify.min.css");

        $this->data['js'][] = base_url("assets/libs/dropzone/min/dropzone.min.js");
        $this->data['js'][] = base_url("assets/libs/dropify/js/dropify.min.js");
        $this->data['js'][] = base_url("assets/custom/js/banner/add-new.js");
        
        isloggedin();
        
        $this->load->templateAdmin('banner/new');
    }

    function edit($id = NULL) {
        if($id):
            $id = decode($id);
        endif;
        
        $this->data['css'][] = base_url("assets/libs/dropzone/min/dropzone.min.css");
        $this->data['css'][] = base_url("assets/libs/dropify/css/dropify.min.css");

        $this->data['js'][] = base_url("assets/libs/dropzone/min/dropzone.min.js");
        $this->data['js'][] = base_url("assets/libs/dropify/js/dropify.min.js");
        $this->data['js'][] = base_url("assets/custom/js/banner/add-new.js");
        
        isloggedin();

        $data = [
            'data' => $this->global_model->_get('banner', ['id' => $id])->row()
        ];
        
        $this->load->templateAdmin('banner/new', $data);
    }

    function delete($id = NULL) {
        if($id):
            $id = decode($id);
        endif;

        $this->global_model->_delete('banner', ['id' => $id]);
        if($this->db->affected_rows() > 0):
			$this->session->set_flashdata('global', get_alert('success', 'Data has been removed.'));
            redirect('banner');
        else:
			$this->session->set_flashdata('global', get_alert('error', 'System error'));
            redirect('banner');
        endif;
    }

    function save() {
        $this->form_validation->set_rules('title-en', 'Title', 'required|max_length[100]');
        $this->form_validation->set_rules('title-ina', 'Title', 'required|max_length[100]');
        $this->form_validation->set_rules('slogan-en', 'Title', 'required|max_length[100]');
        $this->form_validation->set_rules('slogan-ina', 'Title', 'required|max_length[100]');

        if($this->form_validation->run() == true):
            $gambar = $_FILES['gambar']['size'];
            
            $config['upload_path']   = './media/banner/';
            $config['allowed_types'] = 'png|jpg|jpeg';
            $config['max_size']      = 3000;

            $insert = [
                'title' => $this->input->post('title-en'),
                'judul' => $this->input->post('title-ina'),
                'slogan' => $this->input->post('slogan-en'),
                'sloganina' => $this->input->post('slogan-ina'),
            ];
        
            $this->load->library('upload', $config);

            if($gambar <= 0 && !$this->input->post('id')):
                $this->query_error('Please upload 1 picture');
                die();
            elseif ($gambar > 0 && ! $this->upload->do_upload('gambar')):
                $this->query_error($this->upload->display_errors());
                die();
            elseif ($gambar > 0):
                $upload_photo = $this->upload->data();
                $insert['gambar'] = $upload_photo['file_name'];
            endif;

            if($this->input->post('id')):
                $this->global_model->_update('banner', $insert, ['id' => decode($this->input->post('id'))]);
                $save = $this->db->affected_rows();
            else:
                $save = $this->global_model->_insert('banner', $insert);
            endif;

            if($save > 0):
                echo json_encode([
                    'status' => true,
                    'pesan' => 'Data has been updated'
                ]);
            else:
                echo json_encode([
                    'status' => 0,
                    'pesan' => 'System Error'
                ]);
            endif;
        else:
            $this->input_error();
        endif;
    }
}
?>
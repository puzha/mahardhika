<?php
/**
 * Contact Page (Front End)
 */
class Contact extends MY_Controller
{
	
	function __construct() {
		parent::__construct();
	}

	function index() {
		// $data = array();
		$data = [
			'about' => $this->global_model->_get('about', ['id' => '1'])->row(),
		];
		
		$this->load->templateFront('contact-us', $data);
	}
}
?>
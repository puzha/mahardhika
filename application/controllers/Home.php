<?php
/**
 * Home Page (Front End)
 */
class Home extends MY_Controller
{
	
	function __construct() {
		parent::__construct();
	}

	function index() {
		// $data = array();
		$data = [
			'banner' => $this->global_model->_get('banner'),
			'about' => $this->global_model->_get('about', ['id' => '1'])->row(),
			'team' => $this->global_model->_get('team', [], [], [], false, [], 4),
			'article' => $this->global_model->_get('articles', [], [], [], false, [], 3, 'id')
		];
		
		$this->load->templateFront('home', $data);
	}

	function language($lang) {
		$this->session->set_userdata(['language' => $lang]);

		redirect(base_url());
	}
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class : Service
 * By : Puzha Fauzha
 */
class Service extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('service_model');
		isloggedin();
	}

	function index()
	{
		$this->jasa();
	}

	function jasa()
	{
		cekoto('service/jasa','view', true,true );

		$this->load->helper('directory');
		
        $this->data['css'][] = base_url('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-select-bs4/css//select.bootstrap4.min.css');

        $this->data['js'][] = base_url('assets/libs/datatables.net/js/jquery.dataTables.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.html5.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.flash.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.print.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-select/js/dataTables.select.min.js');
        $this->data['js'][] = base_url('assets/libs/pdfmake/build/pdfmake.min.js');
        $this->data['js'][] = base_url('assets/libs/pdfmake/build/vfs_fonts.js');

        $this->data['js'][] = base_url('assets/custom/js/service.js');

        $this->load->model('customer_model');

        $data = array(
        	'category'  => $this->customer_model->getwarna()['query'],
        	'directory' => directory_map(FCPATH . 'upload/files/imports/jasa/'),
        );

		$this->load->templateAdmin('service/jasa-service', $data);
	}

	function jasa_json()
	{
		if($this->input->is_ajax_request())
		{
			$this->load->model('customer_model');
			$category = $this->customer_model->getwarna()['query'];

			$group = $this->service_model->getgroup();

			if($group->num_rows() > 0)
			{
				$data	= array();

				foreach($group->result() as $g)
				{
					$nestedGroup = array(); 

					$nestedGroup[] = '<b>'.$g->nama.'</b>';

					if($category->num_rows() > 0)
					{
						foreach($category->result() as $cat)
						{
							$nestedGroup[] = '';
						}
					}

					$nestedGroup[]	= "<div class='btn-group'>
											<a href='".site_url('service/tambah-group/'.encode($g->id))."' id='EditGroup' class='btn btn-sm btn-info waves-effect waves-light'><i class='fas fa-pen-square'></i></a>
											<a href='".site_url('service/delete-group/'.encode($g->id))."' class='btn btn-sm btn-danger waves-effect waves-light delete'><i class='fas fa-trash-alt'></i></a>
										</div>";

					$data[] = $nestedGroup;

					$requestData	= $_REQUEST;
					$fetch			= $this->service_model->getjasa($requestData['search']['value'], $requestData['order'][0]['column'], $requestData['order'][0]['dir'], $requestData['start'], $requestData['length'], $g->id);
					
					$totalData		= $fetch['totalData'];
					$totalFiltered	= $fetch['totalFiltered'];
					$query			= $fetch['query'];

					foreach($query->result() as $row)
					{ 
						$nestedData = array(); 
						$nestedData[] = '&nbsp; &nbsp; '.$row->nama;

						if($category->num_rows() > 0)
						{
							foreach($category->result() as $cat)
							{
								$nestedData[] = rupiah(sql_get_var('tb_jasa_harga', 'harga', ['jasaid' => $row->id, 'categoryid' => $cat->id]), 0);
							}
						}

						$nestedData[]	= "<div class='btn-group'>
												<a href='".site_url('service/tambah-jasa/'.encode($row->id))."' id='EditJasa' class='btn btn-sm btn-info waves-effect waves-light'><i class='fas fa-pen-square'></i></a>
												<a href='".site_url('service/delete-jasa/'.encode($row->id))."' class='btn btn-sm btn-danger waves-effect waves-light delete'><i class='fas fa-trash-alt'></i></a>
											</div>";

						$data[] = $nestedData;
					}
				}
			}

			$json_data = array(
				"draw"            => intval( $requestData['draw'] ),  
				"recordsTotal"    => intval( $totalData ),  
				"recordsFiltered" => intval( $totalFiltered ), 
				"data"            => $data
				);

			echo json_encode($json_data);
		}
	}

	function tambah_jasa($id = false)
	{
		if($id)
		{
			cekoto('service/jasa','edit', true, false);
		}
		else{
			cekoto('service/jasa','add', true, false);
		}

		if($_POST)
		{
			$this->form_validation->set_rules('nama', 'Nama Jasa', 'required');
			$this->form_validation->set_rules('groupid', 'Group', 'required');

			if($this->form_validation->run() == true)
			{
				$nama    = $this->input->post('nama');
				$groupid = $this->input->post('groupid');

				$newid = newid('tb_jasa');

				$insert = array(
					'nama'    => $nama,
					'groupid' => $groupid,
					'catid'   => 0,
					'harga'   => 0,
				);

				if($id)
				{
					$this->service_model->deleteharga(decode($id));
					$jasaid = decode($id);
					$this->service_model->updatejasa($insert, decode($id));
				}
				else
				{
					$insert['id'] = $newid;
					$jasaid = $newid;

					$this->service_model->addjasa($insert);
				}

				foreach($_POST['harga'] as $groupid => $harga)
				{
					$newhargaid = newid('tb_jasa_harga');

					$this->service_model->addharga([
						'id'         => $newhargaid,
						'jasaid'     => $jasaid,
						'categoryid' => $groupid,
						'harga'      => toFloat($harga),
					]);
				}

				if($this->db->affected_rows() > 0) {
					echo json_encode(array(
						'status' => 1,
						'pesan' => "<i class='fa fa-check' style='color:green;'></i> Data has been updated."
					));
				}
				else
				{
					$this->query_error("Oops, something happends, please contact developer !");
				}
			}
			else
			{
				$this->input_error();
			}
		}
		else
		{
			$this->load->model('customer_model');
			$data['category'] = $this->customer_model->getwarna()['query'];
			$data['group'] = $this->service_model->getgroup();
			if($id)
			{
				$data['jasa'] = $this->service_model->rowjasa(['id' => decode($id)])->row();
			}
			else
			{
				$data['jasa'] = (object) array(
					'id'      => '',
					'nama'    => '',
					'groupid' => 0,
				);
			}
			$this->load->view('popoti/service/add-jasa', $data);
		}
	}

	function delete_jasa($id)
	{

		if(cekoto('service/jasa', 'delete', false, false)):
			if($this->input->is_ajax_request()):
				$this->service_model->updatejasa(['status' => '0'], decode($id));
			endif;
		else:
			$this->query_error('Sorry you don\'t have access to delete this data');
		endif;
	}

	function tambah_group($id = false)
	{
		cekoto('service/jasa','add',true,false);
		if($_POST)
		{
			$this->form_validation->set_rules('nama', 'Group Name', 'required');

			if($this->form_validation->run() == true)
			{
				$nama  = $this->input->post('nama');

				$newid = newid('tb_jasa_group');

				$insert = array(
					'nama'  => $nama,
				);

				if($id)
				{
					$this->service_model->updategroup($insert, decode($id));
				}
				else
				{
					$insert['id'] = $newid;

					$this->service_model->addgroup($insert);
				}

				if($this->db->affected_rows() > 0) {
					echo json_encode(array(
						'status' => 1,
						'pesan' => "<i class='fa fa-check' style='color:green;'></i> Data has been updated."
					));
				}
				else
				{
					$this->query_error("Oops, something happends, please contact developer !");
				}
			}
			else
			{
				$this->input_error();
			}
		}
		else
		{
			if($id)
			{
				$data['group'] = $this->service_model->rowgroup(['id' => decode($id)])->row();
			}
			else
			{
				$data['group'] = (object) array(
					'id'    => '',
					'nama'  => '',
				);
			}
			$this->load->view('popoti/service/add-group', $data);
		}
	}

	function delete_group($id)
	{
		if(cekoto('service/delete-group', 'delete', false, false)):
			if($this->input->is_ajax_request()):
				$this->service_model->updategroup(['status' => '0'], decode($id));
			endif;
		else:
			$this->query_error('Sorry you don\'t have access to delete this data');
		endif;
	}

	function import()
	{
		// print_ar($_POST);
		if($this->input->is_ajax_request())
		{
			cekoto('service/jasa','add', true, false);
			if($_FILES['file']['size'] > 0)
			{
				// first, upload file
				$config['upload_path']   = './upload/files/imports/jasa/';
				$config['allowed_types'] = 'xlsx';
				$config['overwrite']     = true;
				$config['file_name']     = 'PRICELIST : ' .  date('Y-m-d');
		 
				$this->load->library('upload', $config);

				if (! $this->upload->do_upload('file')){
					$this->query_error($this->upload->display_errors());
					die();
				} 

				$data = $this->upload->data();

				include APPPATH.'third_party/PHPExcel/PHPExcel.php';

				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel   = $excelreader->load(FCPATH . 'upload/files/imports/jasa/'.$data['file_name']); // Load file yang tadi diupload ke folder excel
				$sheet       = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet;
				$numrow        = 1;
				$datas         = array();

				$result = '';

				$result .= '<div class="row">';
				$result .= '<div class="col-md-12">';
				$result .= '<i class="fa fa-square mr-1 text-success"></i> New item';
				$result .= '</div>';
				$result .= '<div class="col-md-12">';
				$result .= '<i class="fa fa-square mr-1 text-warning"></i> Item is existing in database and will be replaced with the new price';
				$result .= '</div>';
				$result .= '<div class="col-md-12">';
				$result .= '<i class="fa fa-square mr-1 text-danger"></i> Wrong format';
				$result .= '</div>';
				$result .= '</div>';
				$result .= '<table class="table table-stripped mt-3">';
				$result .= '<thead>';
				$result .= '<tr>';
				$result .= '<td>Service Name</td>';
				$result .= '<td>Category</td>';
				$result .= '<td>Price</td>';
				$result .= '</tr>';
				$result .= '</thead>';
				$result .= '<tbody>';

				$error = 0;
				foreach($sheet as $row)
				{
					$nama     = $row['A'];
					$category = $row['B'];
					$harga    = $row['C'];

					if($numrow > 1)
					{
						// cek keberadaan data:
						$namaExists = $this->service_model->rowjasa(['nama' => $nama])->row()->id;
						$categoryExists = $this->service_model->rowcategory(['nama' => $category])->row()->id;

						if($namaExists)
							$statNama = 'bg-warning';
						else
							$statNama = 'bg-success';

						if($categoryExists)
							$statCategory = '';
						else {
							$error += 1;
							$statCategory = 'bg-danger text-light';
						}

						if(! is_numeric($harga) ) {
							$error += 1;
							$statHarga = 'bg-danger text-light';
						}
						else
							$statHarga = '';

						$result .= '<tr>';
						$result .= '<td class="'.$statNama.'">'.$nama.'</td>';
						$result .= '<td class="'.$statCategory.'">'.$category.'</td>';
						$result .= '<td class="'.$statHarga.'">'.rupiah($harga, 2).'</td>';
						$result .= '</tr>';
					}

					$numrow++;
				}

				$result .= '</tbody>';
				$result .= '</table>';

				echo json_encode([
					'status'    => 1,
					'pesan'     => $result,
					'error'     => $error,
					'file_name' => $data['file_name']
				]);
			}
			else
				$this->load->view('popoti/service/import');
		}
	}

	function init_import()
	{
		if($this->input->is_ajax_request())
		{
			// check file exists
			$filename = $this->input->post('filename');
			if(file_exists(FCPATH . 'upload/files/imports/jasa/'.$filename))
			{
				include APPPATH.'third_party/PHPExcel/PHPExcel.php';

				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel   = $excelreader->load(FCPATH . 'upload/files/imports/jasa/'.$filename); // Load file yang tadi diupload ke folder excel
				$sheet       = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet;
				$numrow        = 1;
				$datas         = array();

				$error = 0;
				foreach($sheet as $row)
				{
					$nama     = $row['A'];
					$category = $row['B'];
					$harga    = $row['C'];

					if($numrow > 1)
					{
						// cek keberadaan data:
						$namaExists = $this->service_model->rowjasa(['nama' => $nama])->row()->id;
						$categoryExists = $this->service_model->rowcategory(['nama' => $category])->row()->id;

						if($namaExists)
							$this->service_model->updatejasa([
								'nama'  => $nama,
								'catid' => $categoryExists->id,
								'harga' => $harga,
							], $namaExists->id);
						else
							$this->service_model->addjasa([
								'nama'  => $nama,
								'catid' => $categoryExists->id,
								'harga' => $harga,
							]);
					}

				}
				echo json_encode([
					'status' => 1,
					'pesan'  => 'your file has been imported',
				]);
			}
			else
			{
				$this->query_error('File not found');
			}
		}
	}

	function category()
	{
        $this->data['css'][] = base_url('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-select-bs4/css//select.bootstrap4.min.css');

        $this->data['js'][] = base_url('assets/libs/datatables.net/js/jquery.dataTables.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.html5.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.flash.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.print.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-select/js/dataTables.select.min.js');
        $this->data['js'][] = base_url('assets/libs/pdfmake/build/pdfmake.min.js');
        $this->data['js'][] = base_url('assets/libs/pdfmake/build/vfs_fonts.js');

        $this->data['js'][] = base_url('assets/custom/js/service.js');

		$this->load->templateAdmin('service/category-service');
	}

	function category_json()
	{
		if($this->input->is_ajax_request())
		{
			$requestData	= $_REQUEST;
			$fetch			= $this->service_model->getcategory($requestData['search']['value'], $requestData['order'][0]['column'], $requestData['order'][0]['dir'], $requestData['start'], $requestData['length']);
			
			$totalData		= $fetch['totalData'];
			$totalFiltered	= $fetch['totalFiltered'];
			$query			= $fetch['query'];

			$data	= array();
			foreach($query->result() as $row)
			{ 
				$nestedData = array(); 

				$nestedData[] = $row->nama;

				$nestedData[]	= "<div class='btn-group'>
										<a href='".site_url('service/tambah-category/'.encode($row->id))."' id='Edit' class='btn btn-info waves-effect waves-light'><i class='fas fa-pen-square mr-1'></i> Edit</a>
										<a href='".site_url('service/delete-category/'.encode($row->id))."' class='btn btn-danger waves-effect waves-light delete'><i class='fas fa-trash-alt mr-1'></i> Delete</a>
									</div>";

				$data[] = $nestedData;
			}

			$json_data = array(
				"draw"            => intval( $requestData['draw'] ),  
				"recordsTotal"    => intval( $totalData ),  
				"recordsFiltered" => intval( $totalFiltered ), 
				"data"            => $data
				);

			echo json_encode($json_data);
		}
	}

	function tambah_category($id = false)
	{
		if($_POST)
		{
			$this->form_validation->set_rules('nama', 'Category Name', 'required');

			if($this->form_validation->run() == true)
			{
				$nama = $this->input->post('nama');

				$newid = newid('tb_service_category');

				$insert = array(
					'nama' => $nama,
				);

				if($id)
				{
					$this->service_model->updatecategory($insert, decode($id));
				}
				else
				{
					$insert['id'] = $newid;

					$this->service_model->addcategory($insert);
				}

				if($this->db->affected_rows() > 0) {
					echo json_encode(array(
						'status' => 1,
						'pesan' => "<i class='fa fa-check' style='color:green;'></i> Data has been updated."
					));
				}
				else
				{
					$this->query_error("Oops, something happends, please contact developer !");
				}
			}
			else
			{
				$this->input_error();
			}
		}
		else
		{
			if($id)
			{
				$data['category'] = $this->service_model->rowcategory(['id' => decode($id)])->row();
			}
			else
			{
				$data['category'] = (object) array(
					'id'   => '',
					'nama' => '',
				);
			}
			$this->load->view('popoti/service/add-category', $data);
		}
	}

	function delete_category($id)
	{
		if($this->input->is_ajax_request())
		{
			$this->service_model->updatecategory(['status' => '0'], decode($id));
		}
	}

	function package()
	{
		cekoto('service/package','view', true, true);
		
        $this->data['css'][] = base_url('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-select-bs4/css//select.bootstrap4.min.css');

        $this->data['js'][] = base_url('assets/libs/datatables.net/js/jquery.dataTables.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.html5.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.flash.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.print.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-select/js/dataTables.select.min.js');
        $this->data['js'][] = base_url('assets/libs/pdfmake/build/pdfmake.min.js');
        $this->data['js'][] = base_url('assets/libs/pdfmake/build/vfs_fonts.js');

        $this->data['js'][] = base_url('assets/custom/js/service.js');

		$this->load->templateAdmin('service/package-service');
	}



	function package_json()
	{
		if($this->input->is_ajax_request())
		{
			$requestData	= $_REQUEST;
			$fetch			= $this->service_model->getpackage($requestData['search']['value'], $requestData['order'][0]['column'], $requestData['order'][0]['dir'], $requestData['start'], $requestData['length']);
			
			$totalData		= $fetch['totalData'];
			$totalFiltered	= $fetch['totalFiltered'];
			$query			= $fetch['query'];

			$data	= array();
			foreach($query->result() as $row)
			{ 
				$nestedData = array(); 

				$jasa = array();
				if($row->detail)
				{
					foreach(json_decode($row->detail) as $jasaid)
					{
						$jasa[] = sql_get_var('tb_jasa_group', 'nama', ['id' => $jasaid]);
					}
				}

				$nestedData[] = $row->nama;
				$nestedData[] = implode(", ", $jasa);
				$nestedData[] = rupiah($row->harga, 2);

				$nestedData[]	= "<div class='btn-group'>
										<a href='".site_url('service/tambah-package/'.encode($row->id))."' id='EditPackage' class='btn btn-info waves-effect waves-light'><i class='fas fa-pen-square mr-1'></i> Edit</a>
										<a href='".site_url('service/delete-package/'.encode($row->id))."' class='btn btn-danger waves-effect waves-light delete'><i class='fas fa-trash-alt mr-1'></i> Delete</a>
									</div>";

				$data[] = $nestedData;
			}

			$json_data = array(
				"draw"            => intval( $requestData['draw'] ),  
				"recordsTotal"    => intval( $totalData ),  
				"recordsFiltered" => intval( $totalFiltered ), 
				"data"            => $data
				);

			echo json_encode($json_data);
		}
	}

	function cancel_order($id)
	{
		if(cekoto('service/summary', 'delete', false, false)):
			if($this->input->is_ajax_request()):
			$id = decode($id);
			$this->global_model->_update('tb_service',['is_cancel' => 1, 'alasan_cancel' => $this->input->post()['reason']]  ,['id' => $id]);
			echo json_encode(['status' => 1]);
			endif;
		else:
			$this->query_error('Sorry you don\'t have access to delete this data');
		endif;
	}

	function tambah_package($id = false)
	{
		if($id)
		{
			cekoto('service/package', 'edit', true, false);
		}
		else
		{
			cekoto('service/package', 'edit', true, false);
		}
		if($_POST)
		{
			$this->form_validation->set_rules('nama', 'Nama Paket', 'required');
			$this->form_validation->set_rules('harga', 'Harga', 'required');

			if($this->form_validation->run() == true)
			{
				$nama   = $this->input->post('nama');
				$harga  = $this->input->post('harga');
				$detail = array();

				if(count($_POST['groupid']) > 0)
				{
					$i = 0;
					foreach($this->input->post('groupid') as $groupid) {
						$detail[] = $groupid;
						$i++;
					}
				}

				$newid = newid('tb_service_package');

				$insert = array(
					'nama'   => $nama,
					'harga'  => toFloat($harga),
					'detail' => json_encode($detail),
				);

				if($id)
				{
					$this->service_model->updatepackage($insert, decode($id));
				}
				else
				{
					$insert['id'] = $newid;

					$this->service_model->addpackage($insert);
				}

				if($this->db->affected_rows() > 0) {
					echo json_encode(array(
						'status' => 1,
						'pesan' => "<i class='fa fa-check' style='color:green;'></i> Data has been updated."
					));
				}
				else
				{
					$this->query_error("Oops, something happends, please contact developer !");
				}
			}
			else
			{
				$this->input_error();
			}
		}
		else
		{
			$data['group'] = $this->service_model->getgroup();
			if($id)
			{
				$data['package'] = $this->service_model->rowpackage(['id' => decode($id)])->row();
			}
			else
			{
				$data['package'] = (object) array(
					'id'     => '',
					'nama'   => '',
					'detail' => '',
					'harga'  => '',
				);
			}
			$this->load->view('popoti/service/add-package', $data);
		}
	}

	function delete_package($id)
	{
		if(cekoto('service/delete-package', 'delete', false, false)):
			if($this->input->is_ajax_request()):
			$this->service_model->updatepackage(['status' => '0'], decode($id));
			endif;
		else:
			$this->query_error('Sorry you don\'t have access to delete this data');
		endif;
	}

	function spk()
	{
		cekoto('service/spk', 'view', true, true);

		$this->data['css'][] = base_url('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-select-bs4/css//select.bootstrap4.min.css');

		$this->data['css'][] = base_url("assets/libs/select2/css/select2.min.css");
        $this->data['css'][] = base_url("assets/libs/selectize/css/selectize.bootstrap3.css");
        $this->data['css'][] = base_url("assets/libs/bootstrap-select/css/bootstrap-select.min.css");
        $this->data['css'][] = base_url("assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css");


        $this->data['js'][] = base_url('assets/libs/datatables.net/js/jquery.dataTables.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.html5.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.flash.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.print.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-select/js/dataTables.select.min.js');
        $this->data['js'][] = base_url('assets/libs/pdfmake/build/pdfmake.min.js');
        $this->data['js'][] = base_url('assets/libs/pdfmake/build/vfs_fonts.js');
        $this->data['js'][] = base_url('assets/libs/bootstrap/js/bootstrap.min.js');
        $this->data['js'][] = base_url("assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js");
        $this->data['js'][] = base_url("assets/js/pages/form-wizard.init.js");
        $this->data['js'][] = base_url("assets/libs/bootstrap-select/js/bootstrap-select.min.js");
        $this->data['js'][] = base_url("assets/libs/devbridge-autocomplete/jquery.autocomplete.min.js");
       	$this->data['js'][] = base_url("assets/libs/select2/js/select2.min.js");
        // $this->data['js'][] = base_url("assets/js/pages/form-advanced.init.js");

        $this->data['js'][] = base_url('assets/custom/js/service.js');
        $data['category_service'] = $this->global_model->_get('tb_customer_category_unit')->result();
		$this->load->templateAdmin('service/spk', $data);
	}
	function dummy_data_for_customer_transaksi()
	{
		$desa = $this->db->get('dis_desa', 10)->result_array();
		$sparepart = $this->db->get('tb_sparepart')->result_array();
		print_r([
			'desa'	=> $desa,
			'sparepart' => $sparepart
		]);
	}
	function parts_json_ajax()
	{
		if($this->input->is_ajax_request())
		{
			$query = $_REQUEST['query'];
			$this->db->select('IFNULL(sum(a.qty), 0) as onhandqty, b.*');
			$this->db->join('tb_sparepart b', 'a.sparepartid = b.id', 'left');
			$this->db->group_by('b.id');
			$this->db->having('onhandqty > 0');
			$customer = $this->db->like('b.nama', $query, 'BOTH')->limit(12)->get('tb_sparepart_location a');
			// echo $this->db->last_query();
			$allitems = array();
			if($customer->num_rows() > 0)
			{
				foreach($customer->result() as $b)
				{
					$allitems[] = [
						'id'		=> $b->id,
						'value'		=> $b->nama,
						'kode'		=> $b->kode,
						'het'		=> $b->het,
						'het1'		=> $b->het1,
						'het2'		=> $b->het2,
						'het3'		=> $b->het3,
						'grosir'	=> $b->grosir,
						'onhandqty'	=> $b->onhandqty,

					];
				}
			}

			$items['query'] = $query;
			$items['suggestions'] = $allitems;
			echo json_encode($items);
		}
	}
	function new_order()
	{
		// if ($this->input->is_ajax_request()) {
			// code...
			// print_ar($this->input->post());die;
			$customer_id     = $this->input->post('customerid');
			$vehicle_id      = $this->input->post('vehicleid');
			$package_service = ($this->input->post('package-service')) ?? false;
			$service_job     = ($this->input->post('service_job')) ?? false;

			$this->form_validation->set_rules('cust-nama', 'Customer Name', 'trim|required|min_length[3]');
			$this->form_validation->set_rules('no-hp', 'No HP', 'trim|required|min_length[3]');
			$this->form_validation->set_rules('district-id', 'Disrict', 'trim|required');
			$this->form_validation->set_rules('no-plat', 'No Plat', 'trim|required|min_length[3]');
			$this->form_validation->set_rules('unit-id', 'Unit', 'trim|required');
			$this->form_validation->set_rules('employeeid', 'Petugas', 'trim|required');

			if ($this->form_validation->run() == true) {
				$service_harga = $this->input->post('service-harga');
				$service_qty   = $this->input->post('service-qty');
				$service_disc  = $this->input->post('service-disc');
				$item_service  = $this->input->post('item_service');

				$paket_harga = $this->input->post('package-harga');
				$paket_qty   = $this->input->post('paket-qty');	
				$paket_disc  = $this->input->post('paket-disc');
				$employee    = $this->input->post('employeeid');

				if ($service_job || $package_service) {
					if (!$customer_id) {
						$dataCustomer['nama']       = $this->input->post('cust-nama'); 
						$dataCustomer['no']         = $this->input->post('no-hp');
						$dataCustomer['desa_id']    = intval($this->input->post('district-id'));
						$dataCustomer['total_come'] = 1;
						$dataCustomer['last_service']  = date('Y-m-d H:s:i');
						$customer_id                = $this->global_model->_insert('tb_customer', $dataCustomer);
					} else {
						$dataCustomer         = [];
						$dataCustomer['nama'] = $this->input->post('cust-nama');
						$dataCustomer['no']   = $this->input->post('no-hp');
						if ($this->input->post('district-id')) {
							$dataCustomer['desa_id'] = intval($this->input->post('district-id'));
						}
					}

					$data = [
						'customer_id' => $customer_id,
						'plat'        => $this->input->post('no-plat'),
						'unit_id'     => $this->input->post('unit-id'),
						'merk_id'     => $this->input->post('merkid'),
						'jenis_id'    => $this->input->post('jenisid'),
						'kategori_id' => $this->input->post('category_id'),
					];

					if (!$vehicle_id) {
						$vehicle_id = $this->global_model->_insert('tb_customer_vehicles', $data);	
					} else {
						$this->global_model->_update('tb_customer_vehicles', $data, ['id' => $vehicle_id]);
					}

					$user = [
						'userid'				=> $this->session->userdata('user'),
						'customer_id' 			=> $customer_id, 
						'vehicle_id' 			=> $vehicle_id, 
						'service_categoryid'	=> $this->input->post('category_id'), 
						'employee_id' 			=> $employee,
						'status'				=> $this->input->post('state'),
						'date'					=> date('Y-m-d H:s:i'),
						'no'					=> getNoFormat('WO'),
						'type'					=> 'service'
					];
					if ($this->input->post('suspend')) {
						$user['suspend'] = $this->input->post('suspend', true);
					}

					if($this->input->post('state') == 1)
						$user['start_time'] = date('Y-m-d H:s:i');

					if($this->input->post('state') == 2)
						$user['finish_time'] = date('Y-m-d H:s:i');

					if($this->input->post('hassave')){
						$service_id = $this->input->post('hassave');
						$this->global_model->_update('tb_service', $user, ['id' => $service_id]);
						$this->service_model->deleteservice($service_id);
					}else{				
						$service_id = $this->global_model->_insert('tb_service', $user);
						$this->service_model->incretotalcome($customer_id,$dataCustomer);
					}

					$jumlah = 0;
					if ($service_job) {
						$jumlah += $this->service_model->addservice($service_job, $item_service, $service_harga, $service_qty, $service_disc, 'item', $service_id);
					}
					if($package_service){
						$jumlah += $this->service_model->addservice($package_service, $item_service, $paket_harga, $paket_qty, $paket_disc, 'package', $service_id);
					}

					if ($this->input->post('parts_id')) {
						$jumlah += $this->service_model->addparts($this->input->post('parts_id'),$this->input->post('part-harga') ,$this->input->post('part-qty'), $this->input->post('part-disc'), $customer_id, $service_id, ($this->input->post('pickingqty')) ?? false);
					}

					updateNo('WO');
					if($this->input->post('state') == 2){				
						addpayment(['tb_service' => $service_id], $jumlah, true, false, 'tb_service','in' ,$customer_id, 0, getNoFormat('FAKTUR'));
						echo json_encode($this->db->last_query());
						updateNo('FAKTUR');
					}
				}else{					
					echo json_encode(['status' => 0, 'pesan' => 'Service Item or Service Package Must Be Required']);
					die;
				}
			} else {
				$this->input_error_without_div();
				die;
			}
			echo json_encode(['service_id' => $service_id, 'jumlah' => $jumlah ]);
		// }

		// echo json_encode($this->input->post());die;

	}

	function package_json_autocomplete()
	{
		// if($this->input->is_ajax_request())
		// {
			$query = $_REQUEST['query'];

			$categoryid = $this->input->post('categoryid');

			$jasa = $this->service_model->getpackagejson($query, $categoryid);
			$items['query'] = $query;
			$items['suggestions'] = $jasa;
			// print_ar($items);die;
			echo json_encode($items);
		// }
	}

	function group_service()
	{
		$this->load->view('popoti/service/group-service');
	}
	function group_service_json()
	{
		if($this->input->is_ajax_request())
		{
			$requestData	= $_REQUEST;
			$fetch			= $this->service_model->getgroupservice($requestData['search']['value'], $requestData['order'][0]['column'], $requestData['order'][0]['dir'], $requestData['start'], $requestData['length']);

			$totalData		= $fetch['totalData'];
			$totalFiltered	= $fetch['totalFiltered'];
			$query			= $fetch['query'];

			$data	= array();
			foreach($query->result() as $row)
			{ 
				$nestedData = array(); 

				$nestedData[] = $row->nama;

				$nestedData[]	= "<a href='javascript:void(0)' class='btn btn-info waves-effect waves-light chooseGroup' data-id='$row->id'><i class='mdi mdi-cursor-pointer mr-1'></i> Choose</a>";

				$data[] = $nestedData;
			}

			$json_data = array(
				"draw"            => intval( $requestData['draw'] ),  
				"recordsTotal"    => intval( $totalData ),  
				"recordsFiltered" => intval( $totalFiltered ), 
				"data"            => $data
			);

			echo json_encode($json_data);
		}
	}
	function selectservecemodal()
	{
		$catid = $this->input->post('categoryid');
		$groupid = $this->input->post('groupid');
		$jasa = $this->service_model->getpackagejsonfrommodal($groupid, $catid);
		echo json_encode($jasa);
	}
	function show_parts()
	{
		cekoto('service/spk', 'view', true, false);
		$data['category'] = $this->global_model->_get('tb_sparepart_category')->result();
		return $this->load->view('popoti/service/show-parts', $data);
	}
	function show_parts_json($cat = '1')
	{
		// if($this->input->is_ajax_request())
		// {
			$islocation = (isset($_GET['location'])) ? true : false;
			$requestData	= $_REQUEST;
			$fetch			= $this->service_model->getpartsjson($requestData['search']['value'], $requestData['order'][0]['column'], $requestData['order'][0]['dir'], $requestData['start'], $requestData['length'], $islocation, $cat);

			$totalData		= $fetch['totalData'];
			$totalFiltered	= $fetch['totalFiltered'];
			$query			= $fetch['query'];

			$data	= array();
			foreach($query->result() as $row)
			{ 
				$nestedData = array(); 

				if (isset($_GET['location'])) {
					$nestedData[] = $row->kode;
				}
				$nestedData[] = $row->nama;

				if (!isset($_GET['location'])) {
					$nestedData[] = $row->stock;
					$nestedData[] = 'Rp. '.number_format($row->het,0,',','.');
				}

				$nestedData[]	= "<a href='javascript:void(0)' class='btn btn-info waves-effect waves-light choosePart' data-id='$row->id'><i class='mdi mdi-cursor-pointer mr-1'></i> Choose</a>";

				$data[] = $nestedData;
			}

			$json_data = array(
				"draw"            => intval( $requestData['draw'] ),  
				"recordsTotal"    => intval( $totalData ),  
				"recordsFiltered" => intval( $totalFiltered ), 
				"data"            => $data
			);

			echo json_encode($json_data);
		// }
	}
	function selectpartsjson()
	{
		if ($this->input->is_ajax_request()) {
			$id = $this->input->post('id');
			$this->db->select('a.*, SUM(b.qty) as onhandqty');
			$this->db->join('tb_sparepart_location b', 'a.id = b.sparepartid', 'left');
			$this->db->join('tb_location c', 'c.id = b.locationid', 'left');

			$data = $this->db->get_where('tb_sparepart a', ['a.id' => $id])->row_array();
			echo json_encode($data);
		}
	}
	function show_paket_service()
	{
		cekoto('service/spk', 'view', true, false);
		return $this->load->view('popoti/service/paket-service');
	}
	function paket_service_json()
	{
		if($this->input->is_ajax_request())
		{
			$requestData	= $_REQUEST;
			$fetch			= $this->service_model->getpackage($requestData['search']['value'], $requestData['order'][0]['column'], $requestData['order'][0]['dir'], $requestData['start'], $requestData['length']);
			
			$totalData		= $fetch['totalData'];
			$totalFiltered	= $fetch['totalFiltered'];
			$query			= $fetch['query'];

			$data	= array();
			foreach($query->result() as $row)
			{ 
				$nestedData = array(); 

				$jasa = array();
				if($row->detail)
				{
					foreach(json_decode($row->detail) as $jasaid)
					{
						$jasa[] = sql_get_var('tb_jasa_group', 'nama', ['id' => $jasaid]);
					}
				}

				$nestedData[] = $row->nama;
				$nestedData[] = implode(", ", $jasa);
				$nestedData[] = rupiah($row->harga, 2);

				$nestedData[]	= "<a href='javascript:void(0)' class='btn btn-info waves-effect waves-light choosePaket' data-id='$row->id'><i class='mdi mdi-cursor-pointer mr-1'></i> Choose</a>";

				$data[] = $nestedData;
			}

			$json_data = array(
				"draw"            => intval( $requestData['draw'] ),  
				"recordsTotal"    => intval( $totalData ),  
				"recordsFiltered" => intval( $totalFiltered ), 
				"data"            => $data
				);

			echo json_encode($json_data);
		}
	}
	function selectpaketservice($value='')
	{
		if ($this->input->is_ajax_request()) {
			$data = $this->global_model->_get('tb_service_package', ['id' => $this->input->post('id')])->row_array();
			$jasa = '';
			foreach(json_decode($data['detail']) as $jasaid)
			{
				$jasa = sql_get_var('tb_jasa', 'nama', ['id' => $jasaid]).', '.$jasa;
			}
			$data['detail2'] = $jasa;	
			echo json_encode($data);
		}
	}
	function autocomplete_paket_service()
	{
		if($this->input->is_ajax_request())
		{
			$query = $_REQUEST['query'];
			$customer = $this->db->like('nama', $query, 'BOTH')->get('tb_service_package');
			// echo $this->db->last_query();die;
			$allitems = array();

			if($customer->num_rows() > 0)
			{
				foreach($customer->result() as $b)
				{
					$jasa = '';
					foreach(json_decode($b->detail) as $jasaid)
					{
						$jasa = sql_get_var('tb_jasa', 'nama', ['id' => $jasaid]).', '.$jasa;
					}
					$allitems[] = array('value' => $b->nama, 'id' => $b->id, 'harga' => $b->harga, 'detail' => $jasa);
				}
			}

			$items['query'] = $query;
			$items['suggestions'] = $allitems;
			echo json_encode($items);
		}
	}
	function retail_sparepart()
	{
		cekoto('service/retail-sparepart', 'view', true, true);

		$this->data['css'][] = base_url('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-select-bs4/css//select.bootstrap4.min.css');

		$this->data['css'][] = base_url("assets/libs/select2/css/select2.min.css");
        $this->data['css'][] = base_url("assets/libs/selectize/css/selectize.bootstrap3.css");
        $this->data['css'][] = base_url("assets/libs/bootstrap-select/css/bootstrap-select.min.css");
        $this->data['css'][] = base_url("assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css");


        $this->data['js'][] = base_url('assets/libs/datatables.net/js/jquery.dataTables.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.html5.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.flash.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.print.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-select/js/dataTables.select.min.js');
        $this->data['js'][] = base_url('assets/libs/pdfmake/build/pdfmake.min.js');
        $this->data['js'][] = base_url('assets/libs/pdfmake/build/vfs_fonts.js');
        $this->data['js'][] = base_url('assets/libs/bootstrap/js/bootstrap.min.js');
        $this->data['js'][] = base_url("assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js");
        $this->data['js'][] = base_url("assets/js/pages/form-wizard.init.js");
        $this->data['js'][] = base_url("assets/libs/bootstrap-select/js/bootstrap-select.min.js");
        $this->data['js'][] = base_url("assets/libs/devbridge-autocomplete/jquery.autocomplete.min.js");
       	$this->data['js'][] = base_url("assets/libs/select2/js/select2.min.js");
        $this->data['js'][] = base_url('assets/custom/js/service.js');
        $this->data['menuName'] = 'Retail Sparepart';

		$this->load->templateAdmin('service/retail');
	}

	function new_retail()
	{
		// echo json_encode($this->input->post());die;
		// echo 'test';die;
		$this->form_validation->set_rules('cust-nama', 'Customer Name', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('no-hp', 'No HP', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('district-id', 'Disrict', 'trim|required');

		if ($this->form_validation->run() == true) {

			if (count($this->input->post('parts_id')) < 1) {
				echo json_encode(['status' => 0, 'pesan' => 'Parts is required']);
				die;
			}

			$customer_id = $this->input->post('customerid');
			if (!$customer_id) {
				$dataCustomer = [
					'nama'			=> $this->input->post('cust-nama'),
					'no'			=> $this->input->post('no-hp'),
					'total_come' 	=> 1,
					'desa_id'		=> $this->input->post('district-id')
				];
				$customer_id = $this->global_model->_insert('tb_customer', $dataCustomer);
			} else {
				$dataCustomer = [];
				$dataCustomer['nama'] = $this->input->post('cust-nama');
				$dataCustomer['no'] = $this->input->post('no-hp');
				if ($this->input->post('district-id')) {
					$dataCustomer['desa_id'] = $this->input->post('district-id');
				}
				$this->service_model->incretotalcome($customer_id,$dataCustomer);
			}
			$user = [
				'userid'				=> $this->session->userdata('user'),
				'customer_id' 			=> $customer_id, 
				'date'					=> date('Y-m-d H:s:i'),
				'no'					=> getNoFormat('WO'),
				'type'					=> 'part'
			];
			$user['status'] = (isset($_POST['status']) ? $this->input->post('status') : '0');
			$service_id = '';
			if ($this->input->post('hassave')) {
				$service_id = $this->input->post('hassave');
				 $this->global_model->_delete('tb_service_parts', ['service_id' => $service_id]);
				 $this->global_model->_update('tb_service', $user, ['id' => $service_id]);
			}else{		
				$service_id = $this->global_model->_insert('tb_service' ,$user);
			}

					updateNo('WO');
			$jumlah = 0;

			if ($this->input->post('parts_id')) {
				// print_ar($this->input->post());
				$jumlah += $this->service_model->addparts($this->input->post('parts_id'),$this->input->post('part-harga') ,$this->input->post('part-qty'), $this->input->post('part-disc'), $customer_id, $service_id, ($this->input->post('pickingqty')) ?? false);
			} 
			if($this->input->post('status') == 1){				
				addpayment(['tb_service' => $service_id], $jumlah, true, false, 'tb_service','in' ,$customer_id, 0, getNoFormat('FAKTUR'));
				updateNo('FAKTUR');
			}
			if ($service_id) {
				echo json_encode(['service_id' => $service_id]);
			}
		}else{
			$this->input_error_without_div();
		}

	}

	function printservice($service_id, $type)
	{
		// print_r($this->session->userdata());die;
		$data = $this->service_model->selectservice($service_id, $type);
		// print_ar($data);die;
		$this->load->templateAdmin('service/cetak', ['data' => $data, 'type' => $type]);
	}
	function printsummary()
	{
		$rows = [];
        for ($i= 0; $i < count($this->input->get('noWo')); $i++) { 
            $rows[$i]['tgl'] = $this->input->get('tgl')[$i];
            $rows[$i]['noWo'] = $this->input->get('noWo')[$i];
            $rows[$i]['cst'] = $this->input->get('cst')[$i];
            $rows[$i]['nominal'] = $this->input->get('nominal')[$i];
            $rows[$i]['status'] = $this->input->get('status')[$i];
        }
        $data['company'] = $this->global_model->_get('pp_settings')->row_array();
        $data['rows'] = $rows;
        $this->load->templateAdmin('service/printsummary', $data);
	}
	function suspend_view()
	{
		return $this->load->view('popoti/service/suspend');
	}

	function summary()
	{
		cekoto('service/summary', 'view', true, true);
		// print_ar(getNoFormat('WO'));die;
		$this->data['css'][] = base_url('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-select-bs4/css//select.bootstrap4.min.css');

		$this->data['css'][] = base_url("assets/libs/select2/css/select2.min.css");
        $this->data['css'][] = base_url("assets/libs/selectize/css/selectize.bootstrap3.css");
        $this->data['css'][] = base_url("assets/libs/bootstrap-select/css/bootstrap-select.min.css");
        $this->data['css'][] = base_url("assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css");


        $this->data['js'][] = base_url('assets/libs/datatables.net/js/jquery.dataTables.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.html5.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.flash.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.print.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-select/js/dataTables.select.min.js');
        $this->data['js'][] = base_url('assets/libs/pdfmake/build/pdfmake.min.js');
        $this->data['js'][] = base_url('assets/libs/pdfmake/build/vfs_fonts.js');
        $this->data['js'][] = base_url('assets/libs/bootstrap/js/bootstrap.min.js');
        $this->data['js'][] = base_url("assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js");
        $this->data['js'][] = base_url("assets/js/pages/form-wizard.init.js");
        $this->data['js'][] = base_url("assets/libs/bootstrap-select/js/bootstrap-select.min.js");
        $this->data['js'][] = base_url("assets/libs/devbridge-autocomplete/jquery.autocomplete.min.js");
       	$this->data['js'][] = base_url("assets/libs/select2/js/select2.min.js");
        $this->data['js'][] = base_url('assets/custom/js/service.js');
        $this->data['menuName'] = 'Summary';

		$this->load->templateAdmin('service/summary');
	}
	public function sumcoba()
	{
		print_ar($this->service_model->summary_data_json());
	}
	function summary_json()
	{

		// if($this->input->is_ajax_request())
		// {
			// print_ar($this->input->post());
			$status = (isset($_POST['status'])) ? $_POST['status'] : null; 
			$tanggal1 = (isset($_POST['tanggal1'])) ? $_POST['tanggal1'] : null; 
			$tanggal2 = (isset($_POST['tanggal2'])) ? $_POST['tanggal2'] : null;
			$requestData	= $_REQUEST;
			$fetch			= $this->service_model->summary_data_json($requestData['search']['value'], $requestData['order'][0]['column'], $requestData['order'][0]['dir'], $requestData['start'], $requestData['length'], $status, $tanggal1, $tanggal2);
			
			$totalData		= $fetch['totalData'];
			$totalFiltered	= $fetch['totalFiltered'];
			$query			= $fetch['query'];

			// echo $this->db->last_query();
			// print_ar($this->db->last_query());
			$data	= array();
			foreach($query->result() as $row)
			{ 
				// var_dump($row->paid);
				$nestedData = array(); 

				$nestedData[] = tgl($row->date);
				$nestedData[] = $row->no;
				// print_ar($row);
				if ($row->is_cancel != 1) {
					if ($row->status == 2 OR ($row->type == 'part' AND $row->status == 1)) {
						$nestedData[] = '<span class="text-center">'.$row->customer.'</span>';
					}else{
						$nestedData[] = '<a href="'.base_url('service/edit_order/'.$row->type.'/'.encode($row->id)).'"  class="btn-link">'.$row->customer.'</a>'; 
					}
				}else{
					$nestedData[] = '<span class="text-center">'.$row->customer.'</span>';
				}




				$jumlah = 0;
				if ($row->detail) {
					$pecah = explode(';,', $row->detail);
					foreach($pecah as $det)
					{
						$det = str_replace(';', '', $det);
						$jumrow = json_decode($det, true);
						if(! isset($jumrow['selling_price'])):
							foreach($jumrow as $d)
							{
								$jumlah += $d['selling_price'];
							}
						else:
							$jumlah += $jumrow['selling_price'];
							endif;
					}
				}

				if($row->parts):
					$dataParts = explode(',', $row->parts);
					foreach($dataParts as $parts):
						$detailPart = explode('-', $parts);

						$het  = $detailPart[0];
						$qty  = $detailPart[1];
						$disc = $detailPart[2];

						$jumlah += ($qty * $het) - $disc;
					endforeach;
				endif;

				$nestedData[] = 'Rp. '.rupiah($jumlah);

				$color = '';
				$text = '';				
				switch ($row->status) {
					case 0: $color = 'warning';$text = 'Suspend';  break;
					case 1: $color = 'primary';$text = 'Waiting For Finish';  break;
					case 2:
						if ($row->paid === '1') {
							$color = 'success';$text = 'Success';  
						}else{
							$color = 'primary';$text = 'Waiting for Payment';  
						}
						break;
					case 3: $color = 'primary';$text = 'Waiting For Start';  break;
				}

				if ($row->is_cancel == 1) {
					$nestedData[] = '<button type="button" class="btn btn-sm btn-danger">Cancel</button>';
				}else{
					if ($row->type == 'service'){
						$nestedData[] = '<button type="button" class="btn btn-sm btn-'.$color.'">'.$text.'</button>';
					}
					else{
						if($row->status == 0){
							$nestedData[] = '<button type="button" class="btn btn-sm btn-light">Retail Waiting For Picking</button>';
						}else if($row->status == 1){
							$nestedData[] = '<button type="button" class="btn btn-sm btn-primary">Retail Waiting For Payment</button>';
						}else if($row->status == 2){
							$nestedData[] = '<button type="button" class="btn btn-sm btn-primary">Retail Waiting For Finish</button>';
						}else if($row->paid){
							$nestedData[] = '<button type="button" class="btn btn-sm btn-success">Retail Success</button>';
						}
					}
				}

				if ($row->is_cancel != 1) {
					if ($row->status == 3) {
						$nestedData[] = '<a href="'.base_url('service/cancel_order/'.encode($row->id)).'" class="btn btn-sm btn-danger cancel-order"><i class="fa fa-trash-alt"></i></a>';
					}else{
						$nestedData[] = '';
					}
				}else{
					$nestedData[] = '';
				}
				
				

				$data[] = $nestedData;
			}


			$json_data = array(
				"draw"            => intval( $requestData['draw'] ),  
				"recordsTotal"    => intval( $totalData ),  
				"recordsFiltered" => intval( $totalFiltered ), 
				"data"            => $data
				);

			echo json_encode($json_data);
		// }
	}

	function hitungjumlah($arr)
	{
		$jum = 0;
		foreach ($arr as $key => $value) {

			print_ar(json_encode($value->selling_price));
			$jum +=  json_decode($value->selling_price);

		}
		return $jum;
	}
	function edit_order($type, $service_id)
	{
		$service_id = decode($service_id);
		$this->data['css'][] = base_url('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css');
        $this->data['css'][] = base_url('assets/libs/datatables.net-select-bs4/css//select.bootstrap4.min.css');

		$this->data['css'][] = base_url("assets/libs/select2/css/select2.min.css");
        $this->data['css'][] = base_url("assets/libs/selectize/css/selectize.bootstrap3.css");
        $this->data['css'][] = base_url("assets/libs/bootstrap-select/css/bootstrap-select.min.css");
        $this->data['css'][] = base_url("assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css");


        $this->data['js'][] = base_url('assets/libs/datatables.net/js/jquery.dataTables.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.html5.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.flash.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-buttons/js/buttons.print.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js');
        $this->data['js'][] = base_url('assets/libs/datatables.net-select/js/dataTables.select.min.js');
        $this->data['js'][] = base_url('assets/libs/pdfmake/build/pdfmake.min.js');
        $this->data['js'][] = base_url('assets/libs/pdfmake/build/vfs_fonts.js');
        $this->data['js'][] = base_url('assets/libs/bootstrap/js/bootstrap.min.js');
        $this->data['js'][] = base_url("assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js");
        $this->data['js'][] = base_url("assets/js/pages/form-wizard.init.js");
        $this->data['js'][] = base_url("assets/libs/bootstrap-select/js/bootstrap-select.min.js");
        $this->data['js'][] = base_url("assets/libs/devbridge-autocomplete/jquery.autocomplete.min.js");
       	$this->data['js'][] = base_url("assets/libs/select2/js/select2.min.js");
    	$this->data['js'][] = base_url('assets/custom/js/service.js');

		$data['user'] = $this->service_model->getuserservice($service_id, $type);	

		if ($type == 'service') {
			$data['service'] = $this->service_model->getservicejasa($service_id);
		}

		$data['part'] = $this->service_model->getservicepart($service_id);
		// print_ar($data);die;
		if($type == 'service'){
			$this->load->templateAdmin('service/edit_service', ['data' => $data]);
		}
		else{
			$this->load->templateAdmin('service/edit_part', ['data' => $data]);
		}
	}
}



?>
<?php
class MY_Controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$site_lang = $this->session->userdata('language');
        if ($site_lang) {
			if ($site_lang == 'EN'):
				$site_lang = 'english';
			else:
				$site_lang = 'indonesia';
			endif;

            $this->lang->load('message', $site_lang);
        } else {
            $this->lang->load('message','english');
        }

		$this->data['service'] = $this->global_model->_get('services');
	}

	function query_error($pesan = "Terjadi kesalahan, coba lagi !")
	{
		$json['status'] = 2;
		$json['pesan'] 	= "<div class='alert alert-danger' role='danger'><i class='mdi mdi-block-helper mr-2'></i>".$pesan."</div>";
		echo json_encode($json);
	}
	
	function input_error()
	{
		$json['status'] = 0;
		$json['pesan'] 	= "<div class=\"alert alert-warning\" role=\"alert\">".validation_errors()."</div>";
		echo json_encode($json);
	}
	function input_error_without_div()
	{
		$json['status'] = 0;
		$json['pesan'] 	= validation_errors();
		echo json_encode($json);
	}

	function clean_tag_input($str)
	{
		$t = preg_replace('/<[^<|>]+?>/', '', htmlspecialchars_decode($str));
		$t = htmlentities($t, ENT_QUOTES, "UTF-8");
		$t = trim($t);
		return $t;
	}
}
<?php
$lang = [
    'banner-title' => 'title',
    'banner-slogan' => 'slogan',
    'about' => 'aboutEN',
    'menu' => [
        'home' => 'Home',
        'profile' => 'Profile',
        'service' => 'Services',
        'article' => 'Articles',
        'contact' => 'Contact',
        'research' => 'Research',
        'consult' => 'Consulting',
        'about' => 'About Us',
        'team' => 'Our Team'
    ],
    'meet-team' => 'Meet Our Team',
    'exclusive-team' => 'We Have An Exclusive<br/>Team Member',
    'service-title' => 'titleEN',
    'service-desc' => 'contentEN',
    'contact-lang' => 'contactEN',
    'visi-misi' => 'visimisiEN',
    'history' => 'historyEN'
];
?>
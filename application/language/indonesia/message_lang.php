<?php
$lang = [
    'banner-title' => 'judul',
    'banner-slogan' => 'sloganina',
    'about' => 'aboutINA',
    'menu' => [
        'home' => 'Home',
        'profile' => 'Profil',
        'service' => 'Layanan',
        'article' => 'Artikel',
        'contact' => 'Hubungi Kami',
        'research' => 'Research',
        'consult' => 'Konsultasi',
        'about' => 'Tentang Kami',
        'team' => 'Tim Kami'
    ],
    'meet-team' => 'Perkenalkan Tim Kami',
    'exclusive-team' => 'Kami Memiliki Anggota Tim<br/>yang Memiliki Kemampuan',
    'service-title' => 'titleINA',
    'service-desc' => 'contentINA',
    'contact-lang' => 'contactINA',
    'visi-misi' => 'visimisiINA',
    'history' => 'historyINA'
];
?>
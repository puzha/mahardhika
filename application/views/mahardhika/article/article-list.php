<?php
if ($articles['query']->num_rows() > 0): 
    foreach($articles['query']->result() as $article):
?>
<!-- Single Post -->
<div class="col-lg-6 col-md-6 col-10 col-tiny-12">
    <div class="single-post-box">
        <div class="post-thumb">
            <img src="<?php echo base_url('media/article/'.$article->gambar) ?>" alt="Image">
        </div>
        <div class="post-content bg-white">
            <span class="post-date"><i class="far fa-calendar-alt"></i><?php echo tgl($article->created_at) ?></span>
            <h3 class="title">
                <a href="#">
                    <?php echo $article->title ?>
                </a>
            </h3>
            <p>
                <?php echo html_cut($article->content, 150) ?>
            </p>
            <a href="#" class="post-link">
                Learn More <i class="far fa-long-arrow-right"></i>
            </a>
        </div>
    </div>
</div>
<!-- Single Post -->
<?php
endforeach;
endif;
?>
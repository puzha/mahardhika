<!--====== Breadcrumb part Start ======-->
<!-- <div class="breadcrumb-section bg-img-c" style="background-image: url(assets/img/breadcrumb.jpg);">
    <div class="container">
        <div class="breadcrumb-text">
            <h1 class="page-title">Blog Grid</h1>
            <ul>
                <li><a href="index.html">Home</a></li>
                <li>Blog</li>
            </ul>
        </div>
    </div>
    <div class="breadcrumb-shapes">
        <div class="one"></div>
        <div class="two"></div>
        <div class="three"></div>
    </div>
</div> -->
<!--====== Breadcrumb part End ======-->

<!--====== Blog Section Start ======-->
<section class="blog-section grey-bg py-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <!-- Blog loop(Grid) -->
                <div class="blog-loop grid-blog row justify-content-center">
                </div>

                <!-- Pagination -->
                <div class="pagination-wrap">
                    <ul>
                        <li><a href="#"><i class="far fa-angle-left"></i></a></li>
                        <li class="active"><a href="#">01</a></li>
                        <li><a href="#">02</a></li>
                        <li><a href="#">03</a></li>
                        <li><a href="#"><i class="far fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-8">
                <!-- sidebar -->
                <div class="sidebar">
                    <!-- Search Widget -->
                    <div class="widget search-widget">
                        <form action="#">
                            <input type="text" placeholder="Search here">
                            <button type="submit"><i class="far fa-search"></i></button>
                        </form>
                    </div>
                    <!-- Cat Widget -->
                    <div class="widget cat-widget">
                        <h4 class="widget-title">Category</h4>

                        <ul>
                            <?php if($categories->num_rows() > 0):
                                foreach($categories->result() as $cat): ?>
                            <li>
                                <a href="<?php echo base_url('article/category/'.encode($cat->id)) ?>"><?php echo $cat->name ?> <span><i class="far fa-angle-right"></i></span></a>
                            </li>
                            <?php endforeach; endif; ?>
                        </ul>
                    </div>
                    <!-- Recent Post Widget -->
                    <div class="widget recent-post-widget">
                        <h4 class="widget-title">Recent News</h4>

                        <div class="post-loops">
                            <div class="single-post">
                                <div class="post-thumb">
                                    <img src="<?php echo base_url() ?>assets/mahardhika/img/sidebar/recent-01.jpg" alt="Image">
                                </div>
                                <div class="post-desc">
                                    <span class="date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                    <a href="#">
                                        Smashing Podcast Epis With Rach Andrewe
                                    </a>
                                </div>
                            </div>
                            <div class="single-post">
                                <div class="post-thumb">
                                    <img src="<?php echo base_url() ?>assets/mahardhika/img/sidebar/recent-02.jpg" alt="Image">
                                </div>
                                <div class="post-desc">
                                    <span class="date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                    <a href="#">
                                        Responsive Web And Desktop Develop
                                    </a>
                                </div>
                            </div>
                            <div class="single-post">
                                <div class="post-thumb">
                                    <img src="<?php echo base_url() ?>assets/mahardhika/img/sidebar/recent-03.jpg" alt="Image">
                                </div>
                                <div class="post-desc">
                                    <span class="date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                    <a href="#">
                                        Django Highlig Models Admin Harnessing
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Popular Tag Widget -->
                    <div class="widget popular-tag-widget">
                        <h4 class="widget-title">Popular Tags</h4>
                        <div class="tags-loop">
                            <a href="#">Business</a>
                            <a href="#">Corporate</a>
                            <a href="#">HTML</a>
                            <a href="#">Finance</a>
                            <a href="#">Investment</a>
                            <a href="#">CSS</a>
                            <a href="#">Planing</a>
                            <a href="#">Creative</a>
                        </div>
                    </div>
                    <!-- Author Infor Widget -->
                    <div class="widget author-widget">
                        <img src="<?php echo base_url() ?>assets/mahardhika/img/sidebar/author.jpg" alt="Image" class="author-img">
                        <h4 class="name">Brandon Johnston</h4>
                        <span class="role">Author</span>
                        <p>
                            Vero eos et accusamus et iustoys odio dignissimos ducimu blanditiis praesentium
                            voluptatum
                        </p>
                        <ul class="social-icons">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-behance"></i></a></li>
                            <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                    <!-- CTA Widget -->
                    <div class="widget cta-widget" style="background-image: url(<?php echo base_url() ?>assets/mahardhika/img/sidebar/cta.jpg);">
                        <h4 class="title">Need Any Consultations</h4>
                        <a href="#" class="main-btn">Send Message</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--====== Blog Section End ======-->
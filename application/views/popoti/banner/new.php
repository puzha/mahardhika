<div class="row">
    <div class="col-md-10" id="add-banner">
        <div class="card">
            <div class="card-body">
                <form id="contextual" method="post" class="col-12">
                    <input type="hidden" value="<?php echo encode($data->id) ?>" name="id" />
                    <div class="row">
                        <div class="col-lg-6">
                            <h4 class="card-title">English Version</h4>
                            <div class="mb-3">
                                <label for="" class="form-label">Title</label>
                                <input type="text" name="title-en" class="form-control" value="<?php echo $data->title ?>">
                            </div>
                            <div class="mb-3">
                                <label for="" class="form-label">Slogan</label>
                                <textarea name="slogan-en" class="form-control"><?php echo $data->slogan ?></textarea>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <h4 class="card-title">Versi Indonesia</h4>
                            <div class="mb-3">
                                <label for="" class="form-label">Judul</label>
                                <input type="text" name="title-ina" class="form-control" value="<?php echo $data->judul ?>">
                            </div>
                            <div class="mb-3">
                                <label for="" class="form-label">Slogan</label>
                                <textarea name="slogan-ina" class="form-control"><?php echo $data->sloganina ?></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="/" method="post" id="myAwesomeDropzone" enctype="multipart/form-data">
                            <input name="gambar" type="file" data-plugins="dropify" data-height="150" <?php echo ($data->gambar) ? 'data-default-file="'.base_url('media/banner/'.$data->gambar).'"' : '' ?> data-allowed-file-extensions="png jpg jpeg" data-max-file-size="3M" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="card">
            <div class="card-body">
                <button class="btn btn-block btn-primary publish-banner"><i class="fe-upload-cloud mr-1"></i>Publish</button>
                <a href="<?php echo base_url('banner') ?>" class="btn btn-block btn-danger"><i class="fe-x mr-1"></i>Cancel</a>
            </div>
        </div>
    </div>
</div>
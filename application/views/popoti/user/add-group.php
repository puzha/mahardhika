<div class="row">

    <div class="col-lg-12">
    	<div class="card">
            <div class="card-body">

                <h4 class="header-title mb-3"> Add New Position <div class="text-primary ml-3" role="status" id="loading"></div></h4>

                <div id="ResponseInput" class="mt-3 mb-3"></div>

                <div id="rootwizard">
                    <ul class="nav nav-pills bg-light nav-justified form-wizard-header mb-3">
                        <li class="nav-item" data-target-form="#posisi">
                            <a href="#first" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                <i class="mdi mdi-account-details mr-1"></i>
                                <span class="d-none d-sm-inline">Posisi</span>
                            </a>
                        </li>
                        <li class="nav-item" data-target-form="#jobdesk">
                            <a href="#second" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                <i class="mdi mdi-clipboard-list mr-1"></i>
                                <span class="d-none d-sm-inline">Job Desk</span>
                            </a>
                        </li>
                        <li class="nav-item" data-target-form="#pengangkatan">
                            <a href="#third" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                <i class="mdi mdi-shield-account mr-1"></i>
                                <span class="d-none d-sm-inline">Surat Pengangkatan</span>
                            </a>
                        </li>
                        <li class="nav-item" data-target-form="#otoritas">
                            <a href="#fourth" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                <i class="mdi mdi-lock-open-check mr-1"></i>
                                <span class="d-none d-sm-inline">Hak Akses</span>
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content mb-0 b-0 pt-0">

                        <div class="tab-pane" id="first">
                            <form id="posisi" method="post" action="<?php echo base_url('user/tambah-posisi/'.encode($otoritas->id)) ?>" class="form-horizontal">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group row mb-3">
                                            <label class="col-md-3 col-form-label" for="userName3">Nama Posisi</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="userName3" name="nama" value="<?php echo $otoritas->nama ?>" required>
                                            </div>
                                        </div>
                                    </div> <!-- end col -->
                                </div> <!-- end row -->
                            </form>
                        </div>

                        <div class="tab-pane fade" id="second">
                        	<form id="jobdesk" method="post" action="<?php echo base_url('user/tambah-posisi/'.encode($otoritas->id)) ?>" enctype="multipart/form-data">
	                            <input name="jobdesk" type="file" required="" data-plugins="dropify" data-height="300" <?php echo ($otoritas->jobdesk) ? 'data-default-file="'.base_url('upload/files/'.$otoritas->jobdesk).'"' : '' ?> data-allowed-file-extensions="pdf" />
	                            <div class="form-group row mt-3">
	                            	<label class="col-form-label">Status Jobdesk</label>
		                            <select class="form-control" name="statusjobdesk">
		                            	<option value="0" <?php echo ($otoritas->statusjobdesk == '0') ? 'selected=""' : '' ?>>Draft</option>
		                            	<option value="1" <?php echo ($otoritas->statusjobdesk == '1') ? 'selected=""' : '' ?>>Publish</option>
		                            </select>
		                        </div>
	                        </form>

                            <!-- <input type="file" data-plugins="dropify" data-default-file="<?php echo base_url('assets/images/small/img-2.jpg') ?>"  /> -->
                        </div>

                        <div class="tab-pane fade" id="third">
                            <form id="pengangkatan" method="post" action="<?php echo base_url('user/tambah-posisi/'.encode($otoritas->id)) ?>" class="form-horizontal">
                                <div class="row">
                                    <div class="col-12">
                                    	<div class="alert alert-info" role="alert">
                                    		Untuk menyisipkan variable, mention list berikut:
                                    		<ul>
                                                <li class="row">
                                                    <span class="col-sm-6 col-md-4 col-lg-2">NIK <small><em class="text-muted">No. Induk</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">nama <small><em class="text-muted">Nama Karyawan</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">panggilan <small><em class="text-muted">Nama Panggilan</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">jk <small><em class="text-muted">Jenis Kelamin</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">noid <small><em class="text-muted">No. Identitas</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">npwp <small><em class="text-muted">NPWP</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">alamat <small><em class="text-muted">Alamat</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">tlp <small><em class="text-muted">No. Tlp</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">email <small><em class="text-muted">Email</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">company <small><em class="text-muted">Penempatan Kerja</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">position <small><em class="text-muted">Jabatan</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">statusKaryawan <small><em class="text-muted">Status Karyawan</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">registeredday <small><em class="text-muted">Tanggal Masuk</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">SK <small><em class="text-muted">No. Surat Keputusan</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">perusahaan <small><em class="text-muted">Nama Perusahaan</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">address <small><em class="text-muted">Alamat Perusahaan</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">web <small><em class="text-muted">Web Perusahaan</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">emailAddress <small><em class="text-muted">Email Perusahaan</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">phone <small><em class="text-muted">No. Tlp Perusahaan</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">fax <small><em class="text-muted">No. Fax Perusahaan</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">pimpinan <small><em class="text-muted">Nama Pimpinan Perusahaan</em></small></span>
                                                    <span class="col-sm-6 col-md-4 col-lg-2">now <small><em class="text-muted">Tanggal Sekarang</em></small></span>
                                                </li>
                                    		</ul>
                                    		Contoh: <strong>@Tanggal Masuk</strong>
                                    	</div>
                                        <div class="form-group row mb-3">
                                        	<textarea required="" id="summernote-pengangkatan"><?php echo $otoritas->pengangkatan ?></textarea>
                                        </div>
                                    </div>
                                    <!-- end col -->
                                </div>
                                <!-- end row -->
                            </form>
                        </div>

                        <div class="tab-pane fade" id="fourth">
                            <form id="otoritas" method="post" action="<?php echo base_url('user/tambah-posisi/'.encode($otoritas->id)) ?>" class="form-horizontal">
                                <div class="row">
                                    <div class="col-12">
                                    	<table class="table table-sm table-striped">
					                        <thead>
					                            <tr>
					                                <th>Pilihan Menu</th>
					                                <th>View</th>
					                                <th>Add</th>
					                                <th>Edit</th>
					                                <th>Delete</th>
					                                <th>Void</th>
					                                <th>Audit</th>
					                                <th>Back Date</th>
					                            </tr>
					                        </thead>
					                        <tbody>
					                            <?php echo nestedTable($menu, FALSE, '', $otoritas->otority) ?>
					                        </tbody>
					                    </table>
                                    </div>
                                    <!-- end col -->
                                </div>
                                <!-- end row -->
                            </form>
                        </div>

                        <ul class="list-inline wizard mb-0">
                            <li class="previous list-inline-item"><a href="javascript: void(0);" class="btn btn-secondary">Previous</a>
                            </li>
                            <li class="next list-inline-item float-right"><a href="javascript: void(0);" class="btn btn-secondary">Next</a></li>
                            <li class="finish list-inline-item float-right"><a href="javascript: void(0);" class="btn btn-secondary">Finish</a></li>
                        </ul>

                    </div> <!-- tab-content -->
                </div> <!-- end #rootwizard-->

            </div> <!-- end card-body -->
        </div> <!-- end card-->
		<!-- <form action="<?php echo site_url('user/tambah-group/'.$otoritas->id) ?>" method="post" enctype="multipart/form-data" class="form-horizontal add-group">
            <div class="row form-group">
                <div class="col col-md-3"><label class=" form-control-label">Nama</label></div>
                <div class="col-12 col-md-9"><input type="text" name="nama" value="<?php echo $otoritas->nama ?>" placeholder="Nama" class="form-control"></div>
            </div>
            <div class="row form-group">
                <div class="col-md-12">
                    
                    <div id='ResponseInput'></div>
                </div>
            </div>
        </form> -->
    </div>

</div>
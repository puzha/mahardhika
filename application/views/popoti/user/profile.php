

                        <div class="row">

                            <div class="col-lg-4 col-xl-4">

                                <div class="card-box text-center">

                                    <form method="post" action="<?php echo base_url('employee/change-avatar') ?>" id="upload-avatar" enctype="multiplart/form-data">

                                        <div class="progress mb-2 mt-2 d-none">

                                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>

                                            </div>

                                        <label for="avatar" style="width: 100%; cursor: pointer;">

                                            <img src="<?php echo base_url('upload/files/'.$employee->photo) ?>" class="rounded-circle avatar-xl img-thumbnail" alt="profile-image">

                                        </label>

                                        <input type="file" name="avatar" id="avatar" class="d-none">

                                    </form>



                                    <h4 class="mb-0"><?php echo $employee->nama ?></h4>

                                    <p class="text-muted"><?php echo $employee->panggilan ?></p>



                                    <div class="text-left mt-3">

                                        <h4 class="font-13 text-uppercase">About Me :</h4>

                                        <p class="text-muted mb-2 font-13"><strong>NIK :</strong> <span class="ml-2"><?php echo $employee->NIK ?></span></p>

                                        <p class="text-muted mb-2 font-13"><strong>Identity Number :</strong> <span class="ml-2"><?php echo $employee->noid ?></span></p>

                                        <p class="text-muted mb-2 font-13"><strong>Full Name :</strong> <span class="ml-2"><?php echo $employee->nama ?></span></p>



                                        <p class="text-muted mb-2 font-13"><strong>Mobile :</strong><span class="ml-2"><?php echo $employee->no ?></span></p>



                                        <p class="text-muted mb-2 font-13"><strong>Email :</strong> <span class="ml-2 "><?php echo $employee->email ?></span></p>

                                        <p class="text-muted mb-2 font-13"><strong>NPWP :</strong> <span class="ml-2 "><?php echo $employee->npwp ?></span></p>

                                        <p class="text-muted mb-2 font-13"><strong>Position :</strong> <span class="ml-2 "><?php echo $employee->position ?></span></p>

                                        <p class="text-muted mb-2 font-13"><strong>Address :</strong> <span class="ml-2 "><?php echo $employee->alamat ?></span></p>



                                    </div>

                                </div> <!-- end card-box -->



                            </div> <!-- end col-->



                            <div class="col-lg-8 col-xl-8">

                                <div class="card-box">



                                    <div class="tab-pane" id="settings">

                                        <form action="<?php echo base_url('employee/save-profile/'.encode($employee->id)) ?>" method="post" id="form-profile">

                                            <h5 class="mb-4 text-uppercase"><i class="mdi mdi-account-circle mr-1"></i> Personal Info</h5>

                                            <div class="row">

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label for="firstname">Name</label>

                                                        <input type="text" class="form-control" id="firstname" name="nama" value="<?php echo $employee->nama ?>" placeholder="Enter first name">

                                                    </div>

                                                </div>

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label for="lastname">Nickname</label>

                                                        <input type="text" class="form-control" id="lastname" name="panggilan" value="<?php echo $employee->panggilan ?>" placeholder="Enter last name">

                                                    </div>

                                                </div> <!-- end col -->

                                            </div> <!-- end row -->



                                            <div class="row">

                                                <div class="col-12">

                                                    <div class="form-group">

                                                        <label for="userbio">Address</label>

                                                        <textarea class="form-control" id="userbio" rows="4" name="alamat" placeholder="Write something..."><?php echo $employee->alamat ?></textarea>

                                                    </div>

                                                </div> <!-- end col -->

                                            </div> <!-- end row -->



                                            <div class="row">

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label for="useremail">Phone Number</label>

                                                        <input type="text" class="form-control" id="useremail" name="no" value="<?php echo $employee->no ?>" placeholder="Enter Phone Number">

                                                    </div>

                                                </div>

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label for="useremail">Email Address</label>

                                                        <input type="email" class="form-control" id="useremail" name="email" value="<?php echo $employee->email ?>" placeholder="Enter email">

                                                    </div>

                                                </div>

                                            </div> <!-- end row -->



                                            <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-office-building mr-1"></i> Company Info</h5>

                                            <div class="row">

                                                <div class="col-md-4">

                                                    <div class="form-group">

                                                        <label for="companyname">Company Name</label>

                                                        <input type="text" class="form-control" id="companyname" value="<?php echo $employee->company ?>" disabled="">

                                                    </div>

                                                </div>

                                                <div class="col-md-4">

                                                    <div class="form-group">

                                                        <label for="cwebsite">Position</label>

                                                        <input type="text" class="form-control" id="position" value="<?php echo $employee->position ?>" disabled="">

                                                    </div>

                                                </div> <!-- end col -->

                                                <div class="col-md-4">

                                                    <div class="form-group">

                                                        <label for="statusKaryawan">Status</label>

                                                        <input 

                                                            type="text" 

                                                            class="form-control" 

                                                            id="statusKaryawan" 

                                                            value="<?php echo ($employee->statusKaryawan == '0') ? 'Training' : $employee == '1' ? 'Kontrak' : 'Tetap'; ?>" 

                                                            disabled=""

                                                        >

                                                    </div>

                                                </div> <!-- end col -->

                                            </div> <!-- end row -->



                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div id="response"></div>

                                                </div>

                                            </div>

                                            

                                            <div class="text-right">

                                                <button type="submit" class="btn btn-success waves-effect waves-light mt-2" id="save-profile"><i class="mdi mdi-content-save"></i> Save</button>

                                            </div>

                                        </form>

                                    </div>

                                    <!-- end settings content-->



                                </div> <!-- end card-box-->



                            </div> <!-- end col -->

                        </div>

                        <!-- end row-->
( function ( $ ) {

    "use strict";

    let base_url = $('#base_url').val();

    $('.inbox-rightbar').html('<i class="fas fa-spin fa-circle-notch mr-1"></i>Please wait ...')
    
    $('.inbox-rightbar').load(base_url + 'articles/article-list')

    $('.list-group-item').on('click', function(e){
        e.preventDefault()

        $('.inbox-rightbar').html('<i class="fas fa-spin fa-circle-notch mr-1"></i>Please wait ...')
        
        $('.inbox-rightbar').load(base_url + 'articles/article-list')
    })
        
})(jQuery)
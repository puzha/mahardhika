( function ( $ ) {

    "use strict";

    var base_url = $('#base_url').val();

    $('.publish-banner').on('click', (e) => {
        e.preventDefault()

        let data = $('#contextual').serializeArray(),
            formData = new FormData($('#myAwesomeDropzone')[0])
        
        data.forEach(p => {
            formData.append(p.name, p.value)
        });

        $.ajax({
            url: base_url + 'banner/save',
            type: 'POST',
            cache: false,
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: () => {
                $('.publish-banner').prop('disabled', true)
                $('.publish-banner').html('<i class="fas fa-spinner mr-1"></i>Publishing ...')
            },
            success: (result) => {
                if(result.status === true) {
                    Swal.fire({
                        title: "Success!",
                        html: result.pesan,
                        type: "success"
                    })

                    window.location.href = base_url + 'banner'
                } else {
                    Swal.fire({
                        title: "Error!",
                        html: result.pesan,
                        type: "error"
                    })
                }
                $('.publish-banner').prop('disabled', false)
                $('.publish-banner').html('<i class="fe-upload-cloud mr-1"></i>Publish')
            }
        })
    })
}) ( jQuery );

! function() {

    "use strict";

}(), 0 < $('[data-plugins="dropify"]').length && $('[data-plugins="dropify"]').dropify({
    messages: {
        default: "Drag and drop a file here or click",
        replace: "Drag and drop or click to replace",
        remove: "Remove",
        error: "Ooops, something wrong appended."
    },
    error: {
        fileSize: "The file size is too big (3M max)."
    }
});
(function ($) {
	'use strict';

    let base_url = $('#base_url').val()

    $('.blog-loop').html('<div><i class="fas fa-spin fa-circle-notch mr-1"></i>Please wait ...</div>')

    $('.blog-loop').load(base_url + 'article/load-article/1')
})(jQuery)